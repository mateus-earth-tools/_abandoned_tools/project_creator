#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : install.sh                                                    ##
##  Project   : project_creator                                               ##
##  Date      : Mar 01, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
ETC_DIR="/usr/local/etc/stdmatt/project_creator";
BIN_DIR="/usr/local/bin"

SCRIPT_DIR=$(pw_get_script_dir);


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##

## Clean the ETC directory.
pw_as_super_user rm    -rf "${ETC_DIR}";
pw_as_super_user mkdir -p  "${ETC_DIR}";


## Install the resources files
pw_as_super_user \
    cp -R "${SCRIPT_DIR}"/etc/* "${ETC_DIR}";

## Install the src files
pw_as_super_user \
    cp -f "${SCRIPT_DIR}"/src/project_creator.py "${BIN_DIR}"/project-creator
